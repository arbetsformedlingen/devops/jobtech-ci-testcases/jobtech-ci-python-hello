#FROM docker.io/library/python:3.13-rc-bookworm
FROM docker.io/library/python:3.13.2-slim-bookworm

COPY Makefile hello_world.py hello_world_test.py .

RUN apt install make \
    make --version \
    make venv deps test
#RUN echo "test"
