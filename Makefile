# setup of local dev env
SHELL := /bin/bash
.PHONY: all

venv:
	python -m venv venv
deps:
	source venv/bin/activate &&  python3 -m pip install pytest pytest-cache pytest-subtests pytest-pylint
test:
	source venv/bin/activate &&   python3 -m pytest -o markers=task hello_world_test.py
image:
	podman build . -t jobtech-ci-python-hello
rmi:
	podman rmi jobtech-ci-python-hello
