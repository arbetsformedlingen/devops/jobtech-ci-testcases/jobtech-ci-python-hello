# jobtech-ci-python-hello
Minimal python testcase for jobtech-ci

repo: https://gitlab.com/arbetsformedlingen/devops/jobtech-ci-testcases/jobtech-ci-python-hello

This is minimal test-case for jobtech-ci.

It is not meant as a pattern for how to do things nicely.

The build happens in the dockerfile. The test happens in the
dockerfile. It also happens in autodevops test stage.

The minimal adjustments to be compatible with auto-devops is a
requirements.txt


Expected CI pipeline status: SUCCESS
